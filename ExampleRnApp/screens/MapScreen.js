import React, {useEffect, useState, useRef} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Body,
  Left,
  Button,
  Right,
  Drawer,
} from 'native-base';
import {default as Icon} from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation, useRoute} from '@react-navigation/native';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {LayerSide} from '../component/LayerSide';

const MapScreen = () => {
  const [mapstyles, setmapstyles] = useState(null);
  const [showModal, setshowModal] = useState(false);
  const drawer = useRef(null);
  const {goBack} = useNavigation();
  const {params} = useRoute();

  useEffect(() => {
    fetch('https://tile.i-bitz.co.th/styles/klokantech-basic/style.json')
      .then((response) => response.json())
      .then((response) => setmapstyles(response))
      .catch((error) => console.log(error));
    return () => {};
  }, []);

  return (
    <Drawer
      ref={drawer}
      onClose={() => {
        drawer.current._root.close();
      }}
      //   openDrawerOffset={0.55}
      //   panCloseMask={0.55}
      //   side="right"
      //   styles={{flex: 1}}
      content={<LayerSide />}>
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={goBack}>
              <Icon name="keyboard-backspace" size={20} />
            </Button>
          </Left>
          <Body>
            <Text>{params.name}</Text>
          </Body>
          <Right>
            <Button
              transparent
              onPress={() => {
                console.log(drawer);
                drawer.current._root.open();
              }}>
              <Icon name="layers" size={20} />
            </Button>
          </Right>
        </Header>
        <View style={styles.container}>
          <MapboxGL.MapView
            style={styles.map}
            styleURL="https://tile.i-bitz.co.th/styles/klokantech-basic/style.json"
            //   attributionEnabled={Boolean(false)}
            logoEnabled={Boolean(false)}
            onDidFinishLoadingMap={(e) => {
              console.log(e);
            }}
          />
        </View>
      </Container>
    </Drawer>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // height: 'calc(100% - 60px)',
    // // width: 300,
    backgroundColor: 'tomato',
  },
  map: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});

export default MapScreen;
