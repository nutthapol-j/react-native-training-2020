import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {default as Icon} from 'react-native-vector-icons/MaterialCommunityIcons';

const DrawerContent = (props) => {
  return (
    <View style={styles.drawerContent}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.headerDrawerSection}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
              }}>
              Example GIS App
            </Text>
          </View>
          <View style={styles.drawerSection}>
            <DrawerItem
              label="Home"
              icon={({color, size}) => {
                return <Icon name="home-outline" color={color} size={size} />;
              }}
              onPress={() => {
                props.navigation.navigate('Home');
              }}
            />
            <DrawerItem
              label="About me"
              icon={({color, size}) => {
                return (
                  <Icon name="contacts-outline" color={color} size={size} />
                );
              }}
              onPress={() => {
                props.navigation.navigate('About');
              }}
            />
          </View>
        </View>
      </DrawerContentScrollView>
      <View style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({color, size}) => (
            <Icon name="exit-to-app" color={color} size={size} />
          )}
          label="Sign Out"
          onPress={() => {
            // signOut();
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  headerDrawerSection: {
    paddingLeft: 20,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
});

export default DrawerContent;
