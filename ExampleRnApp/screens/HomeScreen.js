import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Text,
  Body,
  Title,
  List,
  ListItem,
  Left,
  Right,
  Button,
} from 'native-base';
import {default as Icon} from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';

const arrayProjects = [
  {
    name: 'Colection 1',
    custom_fields: [],
  },
  {
    name: 'Colection 2',
    custom_fields: [],
  },
  {
    name: 'Colection 3',
    custom_fields: [],
  },
];

const HomeScreen = () => {
  const [projects, setprojects] = useState(arrayProjects);

  const {navigate} = useNavigation();

  const deleteRow = (name) => () => {
    setprojects(projects.filter((proj) => proj.name !== name));
  };

  const editProject = (proj) => () => {
    navigate('Map', proj);
  };

  return (
    <Container>
      <Header>
        <Body>
          <Title>Projects</Title>
        </Body>
      </Header>
      <View>
        <List>
          {projects.map((proj) => (
            <ListItem button key={proj.name}>
              <Left>
                <Text>{proj.name}</Text>
              </Left>
              <Right style={styles.rightAction}>
                <Button
                  style={styles.actionBtn}
                  bordered
                  onPress={editProject(proj)}>
                  <Icon name="draw" size={20} color="#007aff" />
                </Button>
                <Button
                  style={styles.actionBtn}
                  bordered
                  danger
                  onPress={deleteRow(proj.name)}>
                  <Icon name="delete" size={20} color="#d9534f" />
                </Button>
              </Right>
            </ListItem>
          ))}
        </List>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  rightAction: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  actionBtn: {width: 45, marginLeft: 8, justifyContent: 'center'},
});

export default HomeScreen;
